const db = uniCloud.database();
module.exports = (code = "fail" , message = "操作失败") => {
	// 返回中间件函数
	return async function auth(ctx, next) {
		const transaction = await db.startTransaction();
		ctx.transaction = transaction ;
		try {
			// 执行后续业务函数
			await next();
			
			//提交事务
			await transaction.commit();
			
		} catch (error) {
			await transaction.rollback();
			ctx.body = {
				code ,
				message : error.message || message
			} ;
			console.error("error =>: ",error);
		}
	};
};
